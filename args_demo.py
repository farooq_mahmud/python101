# demonstrate variable arguments
def make_bread(bread_type, *ingredients):
    if "flour" not in ingredients:
        print("Forgot the flour!")
        return

    print("Bread type: {}".format(bread_type))

    for ingredient in ingredients:
        print(ingredient)

make_bread("wheat", "eggs", "flour", "water", "salt")
make_bread("white", "eggs", "water", "salt")
