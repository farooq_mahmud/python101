def is_palindrome(text):
    lowerCaseText = text.lower()

    # demonstrate multiple assignment and array slicing
    original, rev = lowerCaseText, lowerCaseText[::-1]
    return original == rev


message = "Enter some text and we'll tell you if it's a palindrome\n"
text = input(message)

while len(text) == 0:
    text = input(message)

if is_palindrome(text):
    print("\"{}\" IS a palindrome.".format(text))
else:
    print("\"{}\" IS NOT a palindrome.".format(text))
