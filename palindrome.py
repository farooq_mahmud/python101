# demonstrate functions
def is_palindrome(text):
    lowerCaseText = text.lower()

    head = 0
    tail = len(text) - 1

    while head < tail:
        # demonstrate working with arrays and conditionals
        if lowerCaseText[head] != lowerCaseText[tail]:
            return False
        else:
            # demonstrate operators
            head += 1
            tail -= 1
    return True

message = "Enter some text and we'll tell you if it's a palindrome\n"
text = input(message)

while len(text) == 0:
    text = input(message)

# demonstrate string interpolation
if is_palindrome(text):
    print("\"{}\" IS a palindrome.".format(text))
else:
    print("\"{}\" IS NOT a palindrome.".format(text))
