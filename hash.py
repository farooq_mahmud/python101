# demonstrate importing external code
import hashlib

# demonstrate console input and variable assignment
message = "Enter some text to hash...\n"
text = input(message)

# demonstrate loops
while len(text) == 0:
    text = text = input(message)

# demonstrate calling external code
hashedText = hashlib.md5(text.encode("utf-8")).hexdigest()

# demonstrate console output
print(hashedText)
