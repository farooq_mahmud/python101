# demonstrate keyword arguments
def make_bread(bread_type, **ingredients):
    print(bread_type)
    print("Ingredients:")

    #demonstrate iterating a dictionary
    for item in ingredients.items():
        print("\t{} {}".format(item[1], item[0]))

make_bread("wheat bread", flour="3 cups", water="10 oz", salt="1 tsp")
make_bread("salty bread", flour="2 cups", water="12 oz", salt="2 tbsp", yeast="1 tsp", bake_time_in_hours=6)
